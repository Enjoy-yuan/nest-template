// config/db.ts
const productConfig = {
  mysql: {
    port: 3306,
    host: 'rm-bp16g8j1o2ey3a90y.mysql.rds.aliyuncs.com',
    user: 'yuancheng',
    password: 'w1582682448W',
    database: 'nest_test', // 库名
    connectionLimit: 10, // 连接限制
  },
};

const localConfig = {
  mysql: {
    port: 3306,
    host: 'rm-bp16g8j1o2ey3a90ybo.mysql.rds.aliyuncs.com',
    user: 'yuancheng',
    password: 'w1582682448W',
    database: 'nest_test', // 库名
    connectionLimit: 10, // 连接限制
  },
};

// 本地运行是没有 process.env.NODE_ENV 的，借此来区分[开发环境]和[生产环境]
const config = process.env.NODE_ENV ? productConfig : localConfig;

console.log('当前环境：' + process.env.NODE_ENV);

export default config;

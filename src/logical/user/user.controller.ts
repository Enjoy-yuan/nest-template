import { Controller, Get, Post, Body, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { AuthService } from '../auth/auth.service';
import { UserService } from './user.service';
import {
  ApiOperation,
  ApiProperty,
  ApiTags,
  ApiBearerAuth,
} from '@nestjs/swagger';

class RegisterDto {
  @ApiProperty({ description: '用户名称' })
  readonly username: string;
  @ApiProperty({ description: '用户密码' })
  readonly password: string;
}

class RemoveDto {
  @ApiProperty({ description: '用户名称' })
  readonly username: string;
}
@ApiTags('角色')
@Controller('user')
export class UserController {
  constructor(
    private readonly authService: AuthService,
    private readonly usersService: UserService,
  ) {}

  @Post('login')
  @ApiOperation({ summary: '用户登录' })
  async login(@Body() loginParmas: RegisterDto) {
    console.log('JWT验证 - Step 1: 用户请求登录');
    const authResult = await this.authService.validateUser(
      loginParmas.username,
      loginParmas.password,
    );
    if (!loginParmas.username) {
      return {
        code: 400,
        msg: '用户名称不能为空',
      };
    }
    if (!loginParmas.password) {
      return {
        code: 400,
        msg: '用户密码不能为空',
      };
    }
    switch (authResult.code) {
      case 200:
        return this.authService.certificate(authResult.user);
      case 400:
        return {
          code: 400,
          msg: `账号或密码不正确`,
        };
      default:
        return {
          code: 400,
          msg: `账号或密码不正确`,
        };
    }
  }

  @Post('register')
  @ApiOperation({ summary: '用户注册' })
  async register(@Body() body: RegisterDto) {
    return await this.usersService.register(body);
  }

  @ApiBearerAuth() // Swagger 的 JWT 验证
  @Get('list')
  @UseGuards(AuthGuard('jwt')) // 使用 'JWT' 进行验证
  @ApiOperation({ summary: '用户列表' })
  async list() {
    return await this.usersService.list();
  }

  @ApiBearerAuth() // Swagger 的 JWT 验证
  @Post('remove')
  @UseGuards(AuthGuard('jwt')) // 使用 'JWT' 进行验证
  @ApiOperation({ summary: '删除用户' })
  async remove(@Body() body: RemoveDto) {
    return await this.usersService.remove(body);
  }
}

import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as express from 'express';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.use(express.json()); // For parsing application/json
  app.use(express.urlencoded({ extended: true })); // For parsing application/x-www-form-urlencoded
  app.setGlobalPrefix('nest-api');
  const config = new DocumentBuilder()
    .addBearerAuth() // 开启 BearerAuth 授权认证
    .setTitle('Nest接口') // 标题
    .setDescription('接口api') // 描述
    .setVersion('1.0') // 版本
    // .addTag('cats') // 标签
    .build();
  const document = SwaggerModule.createDocument(app, config);

  SwaggerModule.setup('api-docs', app, document); // 文档前缀路径
  await app.listen(3000);
  console.log('接口地址：http://localhost:3000/api-docs');
}
bootstrap();
